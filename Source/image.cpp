#include "image.h"
#include <ctime>
#include <iomanip>
#include <math.h>
#include <iostream>

void printProgress(int k, double duration, double height){
    float progress = ((float)k) /height;
    int barWidth = 70;

    std::cout << "[";
    int pos = barWidth * progress;
    for (int i = 0; i < barWidth; ++i) {
        if (i < pos) std::cout << "=";
        else if (i == pos) std::cout << ">";
        else std::cout << " ";}

    std::cout << "] " << int(progress * 100) << "%. Time Remaining: "<< std::setprecision(2);
    if (duration * (height -k) < 1){std::cout <<"Finishing.     \r";}
    else if (duration * (height - k) < 100){std::cout << duration * (height-k) << " seconds     \r";}
    else if (duration * (height - k) < 6000){std::cout << (duration * (height - k) / 60) << " minutes    \r";} 
    else { std::cout << "> 1.5 hrs    \r";}
    std::cout.flush();
}


enum {
    BOTH,
    LEFTONLY,
    TOPONLY
};

/**
 * Image
 **/
Image::Image (int width_, int height_) {
    assert(width_ > 0);
    assert(height_ > 0);

    width           = width_;
    height          = height_;
    num_pixels      = width * height;
    pixels          = new Pixel[num_pixels];

    assert(pixels != NULL);
}


Image::Image (const Image& src) {
    width           = src.width;
    height          = src.height;
    num_pixels      = width * height;
    pixels          = new Pixel[num_pixels];

    assert(pixels != NULL);
    memcpy(pixels, src.pixels, src.width * src.height * sizeof(Pixel));
}


Image::~Image () {
    delete [] pixels;
    pixels = NULL;
}

void Image::Update(FIBITMAP* bitmap){
    double total = 0.0;
    for (int y = 0; y < height; y++){
        BYTE* bits = (BYTE *) FreeImage_GetScanLine(bitmap, y);
        for (int x = 0; x < width; x++){
            Pixel * p = &GetPixel(x, y);
            int r = bits[3*x];
            int g = bits[3*x +1];
            int b = bits[3*x +2];
            p->SetClamp(r, g, b);
            total += (double) p->Luminance();
        }
    }
    averageLum = total / (double) NumPixels();
}

unsigned char * Image::GetBytes(){
    unsigned char * bytes = new unsigned char[width * height * 3];

    for (int i = 0; i < width * height; i++){
        bytes[i * 3 + 0] = pixels[i].r;
        bytes[i * 3 + 1] = pixels[i].g;
        bytes[i * 3 + 2] = pixels[i].b;
    }
    return bytes;
}

Image * Image::cropImage(Image * img, int w, int h) {
    Image * i = new Image(w, h);
    Pixel *p;

    // Iterate through new image and set each pixel to proper cropped pixel from current image
    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            p = &(i->GetPixel(x, y)); 
            *p = img->GetPixel(x, y);
        }
    }

    return i;
}

Image * Image::perfectImage(int w, int h, int window){
    int neww = w;
    int newh = h;
    if (w % window != 0) {
        neww = ((w / window) + 1) * window;
    }

    if (h % window != 0) {
        newh = ((h / window) + 1) * window;
    }

    //Create new image
    return new Image(neww, newh);
}

Image * Image::perfectLayeredImage(int w, int h, int window, int overlap, int nonOverlap) {
    // Number of times nonOverlap portion fits into width and height
    int nonOverlapW = w / nonOverlap;
    int nonOverlapH = h / nonOverlap;

    // Holds new width and height to use
    int neww = w;
    int newh = h;

    // If the fit plus overlap is less than width, scale up to next perfect width
    if ((nonOverlapW * nonOverlap) + overlap < w) {
       neww = ((nonOverlapW + 1) * nonOverlap) + overlap;
    }

    // If the fit plus overlap is greater than width, use current perfect width
    else if ((nonOverlapW * nonOverlap) + overlap > w) {
       neww = (nonOverlapW * nonOverlap) + overlap;
    }

    // If the fit plus overlap is less than height, scale up to next perfect height
    if ((nonOverlapH * nonOverlap) + overlap < h) {
       newh = ((nonOverlapH + 1) * nonOverlap) + overlap;
    }

    // If the fit plus overlap is greater than height, scale up to next perfect height
    else if ((nonOverlapH * nonOverlap) + overlap > h) {
       newh = ((nonOverlapH) * nonOverlap) + overlap;
    }

    fprintf(stderr, "%i %i\n", neww, newh);
    return new Image(neww, newh);
}


Pixel * Image::getBlock(int x, int y, int window){
    Pixel * p = new Pixel[window * window];
    for (int i = 0; i < window; i++){
        for (int k = 0; k < window; k++){
            p[i * window + k] = GetPixel((x + k) % width, (i + y) % height);
        }
    }

    return p;
}

/*
 * Adds given block to given block index in image
 */
void Image::addBlock(Pixel * block, int xIndex, int yIndex, int window) {
    for (int i = 0; i < window; i++) {
        for (int k = 0; k < window; k++) {
            Pixel *p = &GetPixel(xIndex * window + k, yIndex * window + i);
            Pixel newP = block[window * window - (i * window + k) - 1];
            p->SetClamp(newP.r, newP.g, newP.b);
        }
    }
}

void Image::addLayeredBlock(Pixel *block, int xIndex, int yIndex, int window, int type, int overlap, int nonOverlap) {
    if (type == TOPONLY) {
        for (int i = overlap; i < window; i++){
            for (int k = 0; k < window; k++){
                Pixel *p = &GetPixel(k, yIndex * nonOverlap + i);
                Pixel newP = block[window * window - (i * window + k + 1)];
                p->SetClamp(newP.r, newP.g, newP.b);
            }
        }

    }
    if (type == LEFTONLY) {
        for (int i = 0; i < window; i++){
            for (int k = overlap; k < window; k++){
                Pixel *p = &GetPixel(xIndex * nonOverlap + k, i);
                Pixel newP = block[window * window - (i * window + k + 1)];
                p->SetClamp(newP.r, newP.g, newP.b);
            }
        }

    }
    if (type == BOTH) {
        for (int i = overlap; i < window; i++){
            for (int k = overlap; k < window; k++){
                Pixel *p = &GetPixel(xIndex * nonOverlap + k, yIndex * nonOverlap + i);
                Pixel newP = block[window * window - (i * window + k + 1)];
                p->SetClamp(newP.r, newP.g, newP.b);
            }
        }
    }
}

void Image::addCutLayeredBlock(Pixel *block, int xIndex, int yIndex, int window, int type, int overlap, int nonOverlap){
    if (type == TOPONLY) {
        // Create 2D array to store errors for dynamic programming
        float errors[window][overlap];

        // Find errors of farthest right column first for basis of cut
        for (int i = overlap - 1; i >= 0; i--) {
            errors[window - 1][i] = L2norm(GetPixel(window - 1, yIndex * nonOverlap + i), block[i * window + (window - 1)]);
        }

        // Iterate over each column (descending order)
        for (int k = window - 2; k >= 0; k--) { 
            // Iterate over each pixel in column
            for (int i = overlap - 1; i >= 0; i--) {
                // Calculate minimum error based on e + min((x + 1, y - 1), (x + 1, y), (x + 1, y + 1)) and store in array
                float min;
                if (i == 0) min = std::min (errors[k + 1][i], errors[k + 1][i + 1]);
                else if (i == overlap - 1) min = std::min (errors[k + 1][i - 1], errors[k + 1][i]);
                else min = std::min (errors[k - 1][i - 1], std::min(errors[k + 1][i], errors[k + 1][i + 1]));

                errors[k][i] = L2norm(GetPixel(k, yIndex * nonOverlap + i), block[i * window + k]) + min;
            }
        }


        // Backtrack through errors array
        // Find the ending point at first column to backtrack with
        float min = std::numeric_limits<float>::infinity();
        int mindex;
        for (int i = 0; i < overlap; i++) {
            if (errors[0][i] < min) {
                min = errors[0][i];
                mindex = i;
            }
        }

        // Add first column of block based on cut to image
        for (int i = mindex; i < window; i++) {
            Pixel *p = &GetPixel(0, yIndex * nonOverlap + i);
            Pixel newP = block[window * window - (i * window + 0 + 1)];
            p->SetClamp(newP.r, newP.g, newP.b);
        }

        // Iterate over the columns
        for (int k = 1; k < window; k++) {
            // Check the minimum of the three valid next pixels in the column
            float min = std::numeric_limits<float>::infinity();
            int mindex2;
            if (mindex > 0 && errors[k][mindex - 1] < min) {
                min = errors[k][mindex - 1];
                mindex2 = mindex - 1;
            }
            if (errors[k][mindex] < min) {
                min = errors[k][mindex];
                mindex2 = mindex;
            }
            if (mindex < overlap - 1 && errors[k][mindex + 1] < min) {
                min = errors[k][mindex + 1];
                mindex2 = mindex + 1;
            }

            mindex = mindex2;

            // Add column of block based on cut to image
            for (int i = mindex; i < window; i++) {
                Pixel *p = &GetPixel(k, yIndex * nonOverlap + i);
                Pixel newP = block[window * window - (i * window + k + 1)];
                p->SetClamp(newP.r, newP.g, newP.b);
            }
        }
    }
    if (type == LEFTONLY) {
        // Create 2D array to store errors for dynamic programming
        float errors[overlap][window];

        // Find errors of farthest right column first for basis of cut
        for (int i = overlap - 1; i >= 0; i--) {
            errors[i][window - 1] = L2norm(GetPixel(xIndex * nonOverlap + i, window - 1), block[(window - 1) * window + i]);
        }

        // Iterate over each row (descending order)
        for (int k = window - 2; k >= 0; k--) { 
            // Iterate over each pixel in row
            for (int i = overlap - 1; i >= 0; i--) {
                // Calculate minimum error based on e + min((x - 1, y + 1), (x, y + 1), (x + 1, y + 1)) and store in array
                float min;
                if (i == 0) min = std::min(errors[i][k + 1], errors[i + 1][k + 1]);
                else if (i == overlap - 1) min = std::min(errors[i - 1][k + 1], errors[i][k + 1]);
                else min = std::min (errors[i - 1][k + 1], std::min(errors[i][k + 1], errors[i + 1][k + 1]));

                errors[i][k] = L2norm(GetPixel(xIndex * nonOverlap + i, k), block[k * window + i]) + min;
            }
        }


        // Backtrack through errors array
        // Find the ending point at first row to backtrack with
        float min = std::numeric_limits<float>::infinity();
        int mindex;
        for (int i = 0; i < overlap; i++) {
            if (errors[i][0] < min) {
                min = errors[i][0];
                mindex = i;
            }
        }

        // Add first row of block based on cut to image
        for (int i = mindex; i < window; i++) {
            Pixel *p = &GetPixel(xIndex * nonOverlap + i, 0);
            Pixel newP = block[window * window - i - 1];
            p->SetClamp(newP.r, newP.g, newP.b);
        }

        // Iterate over the rows
        for (int k = 1; k < window; k++) {
            // Check the minimum of the three valid next pixels in the row
            float min = std::numeric_limits<float>::infinity();
            int mindex2;
            if (mindex > 0 && errors[mindex - 1][k] < min) {
                min = errors[mindex - 1][k];
                mindex2 = mindex - 1;
            }
            if (errors[mindex][k] < min) {
                min = errors[mindex][k];
                mindex2 = mindex;
            }
            if (mindex < overlap - 1 && errors[mindex + 1][k] < min) {
                min = errors[mindex + 1][k];
                mindex2 = mindex + 1;
            }

            mindex = mindex2;

            // Add row of block based on cut to image
            for (int i = mindex; i < window; i++) {
                Pixel *p = &GetPixel(xIndex * nonOverlap + i, k);
                Pixel newP = block[window * window - (k * window + i + 1)];
                p->SetClamp(newP.r, newP.g, newP.b);
            }
        }            
    }
    if (type == BOTH) {
        // Create 2D array to store errors for dynamic programming
        float errorsT[window][overlap];
        float errorsL[overlap][window];

        // Find errors of farthest right column first for basis of cut
        for (int i = overlap - 1; i >= 0; i--) {
            errorsT[window - 1][i] = L2norm(GetPixel(window - 1, yIndex * nonOverlap + i), block[i * window + (window - 1)]);
            errorsL[i][window - 1] = L2norm(GetPixel(xIndex * nonOverlap + i, window - 1), block[(window - 1) * window + i]);
        }

        // Iterate over each column (descending order)
        for (int k = window - 2; k >= 0; k--) { 
            // Iterate over each pixel in column
            for (int i = overlap - 1; i >= 0; i--) {
                // Calculate minimum error based on
                // e + min((x + 1, y - 1), (x + 1, y), (x + 1, y + 1)) for errorsT and
                // e + min((x - 1, y + 1), (x, y + 1), (x + 1, y + 1)) for errorsL 
                // and store in array
                float minT;
                if (i == 0) minT = std::min (errorsT[k + 1][i], errorsT[k + 1][i + 1]);
                else if (i == overlap - 1) minT = std::min (errorsT[k + 1][i - 1], errorsT[k + 1][i]);
                else minT = std::min (errorsT[k + 1][i - 1], std::min(errorsT[k + 1][i], errorsT[k + 1][i + 1]));

                errorsT[k][i] = L2norm(GetPixel(k, yIndex * nonOverlap + i), block[i * window + k]) + minT;

                float minL;
                if (i == 0) minL = std::min (errorsL[i][k + 1], errorsL[i + 1][k + 1]);
                else if (i == overlap - 1) minL = std::min (errorsL[i - 1][k + 1], errorsL[i][k + 1]);
                else minL = std::min (errorsL[i - 1][k + 1], std::min(errorsL[i][k + 1], errorsL[i + 1][k + 1]));

                errorsL[i][k] = L2norm(GetPixel(xIndex * nonOverlap + i, k), block[k * window + i]) + minL;
            }
        }


        // Backtrack through errors array
        // Find the ending point at first column and row to backtrack with
        float cuts[overlap][2];
        float minT = std::numeric_limits<float>::infinity();
        int mindexT;
        float minL = std::numeric_limits<float>::infinity();
        int mindexL;
        for (int i = 0; i < overlap; i++) {
            if (errorsT[0][i] < minT) {
                minT = errorsT[0][i];
                mindexT = i;
            }
            if (errorsL[i][0] < minL) {
                minL = errorsL[i][0];
                mindexL = i;
            }
        }

        // Add first column and row indices to cuts
        cuts[0][0] = mindexT;
        cuts[0][1] = mindexL;
        /*
        // Add first column of block based on cut to image
        for (int i = mindexT; i < window; i++) {
            Pixel *p = &GetPixel(xIndex * nonOverlap, yIndex * nonOverlap + i);
            Pixel newP = block[i * window + 0];
            p->SetClamp(newP.r, newP.g, newP.b);
        }
        // Add first row of block based on cut to image
        for (int i = mindexL; i < window; i++) {
            Pixel *p = &GetPixel(xIndex * nonOverlap + i, yIndex * nonOverlap);
            Pixel newP = block[i];
            p->SetClamp(newP.r, newP.g, newP.b);
        }*/

        // Iterate over the columns and rows
        for (int k = 1; k < window; k++) {
            // Check the minimum of the three valid next pixels in the column
            float minT = std::numeric_limits<float>::infinity();
            int mindex2T;
            if (mindexT > 0 && errorsT[k][mindexT - 1] < minT) {
                minT = errorsT[k][mindexT - 1];
                mindex2T = mindexT - 1;
            }
            if (errorsT[k][mindexT] < minT) {
                minT = errorsT[k][mindexT];
                mindex2T = mindexT;
            }
            if (mindexT < overlap - 1 && errorsT[k][mindexT + 1] < minT) {
                minT = errorsT[k][mindexT + 1];
                mindex2T = mindexT + 1;
            }

            mindexT = mindex2T;


            // Check the minimum of the three valid next pixels in the row
            float minL = std::numeric_limits<float>::infinity();
            int mindex2L;
            if (mindexL > 0 && errorsL[mindexL - 1][k] < minL) {
                minL = errorsL[mindexL - 1][k];
                mindex2L = mindexL - 1;
            }
            if (errorsL[mindexL][k] < minL) {
                minL = errorsL[mindexL][k];
                mindex2L = mindexL;
            }
            if (mindexL < overlap - 1 && errorsL[mindexL + 1][k] < minL) {
                minL = errorsL[mindexL + 1][k];
                mindex2L = mindexL + 1;
            }

            mindexL = mindex2L;
             
            if (k < overlap) {
                cuts[k][0] = mindexT;
                cuts[k][1] = mindexL;
            }

            else {
                // Add column of block based on cut to image
                for (int i = mindexT; i < window; i++) {
                    Pixel *p = &GetPixel(xIndex * nonOverlap + k, yIndex * nonOverlap + i);
                    Pixel newP = block[window * window - (i * window + k + 1)];
                    p->SetClamp(newP.r, newP.g, newP.b);
                }

                // Add row of block based on cut to image
                for (int i = mindexL; i < window; i++) {
                    Pixel *p = &GetPixel(xIndex * nonOverlap + i, yIndex * nonOverlap + k);
                    Pixel newP = block[window * window - (k * window + i + 1)];
                    p->SetClamp(newP.r, newP.g, newP.b);
                }
            }
        }
        
        // Add blocks into double overlap area
        for (int i = 0; i < overlap; i++) {
            for (int k = 0; k < overlap; k++) {
                if (k > cuts[i][1] && i > cuts[k][0]) {
                    Pixel *p = &GetPixel(xIndex * nonOverlap + k, yIndex * nonOverlap + i);
                    Pixel newP = block[window * window - (i * window + k + 1)];
                    p->SetClamp(newP.r, newP.g, newP.b);
                }
            }
        }
    }
}

Image* Image::randomQuilt(int window, int newWidth, int newHeight) {

    // find newnewWidth and newnewHeight
    Image * img = perfectImage(newWidth, newHeight, window);

    for (int i = 0; i < img->height / window; i++){
        for (int k = 0; k  < img->width / window; k++){
            // Choose random pixel
            int x = rand() % (width - window); // Original width and height
            int y = rand() % (height - window);

            // Create block
            Pixel* block = getBlock(x, y, window);

            // Add block to new image
            img->addBlock(block, k, i, window);
        }
    }

    return cropImage(img, newWidth, newHeight);
    // Profit
}

float Image::findError(Pixel * b1, Pixel * b2, int type, int window, int nonOverlap) {
    float totalError = 0.0f;

    if (type == LEFTONLY) {
        // Iterate over y (b1)
        for (int i = 0; i < window; i++) {
            int k = 0; // Keep track of b2's x position
            // Iterate over x (b1)
            for (int j = nonOverlap; j < window; j++) {
                totalError += L2norm(b1[(i * window) + j], b2[(i * window) + k]);
                k++;
            }
        }
    }

    else if (type == TOPONLY) {
        int k = 0; // Keep track of b2's y position
        // Iterate over y (b1)
        for (int i = nonOverlap; i < window; i++) {
            // Iterate over x (b1)
            for (int j = 0; j < window; j++) {
                totalError += L2norm(b1[(i * window) + j], b2[(k * window) + j]);
            }

            k++;
        }
    }

    return totalError;
}

float findLuminance(Pixel * block, Image * tranimg, int x, int y, int window, float averageLum, float overlap, float nonOverlap){
    float luminance = 0.0f;
    bool avg = false;
    float plum, qlum;

    int xpos = x * nonOverlap + overlap;
    int ypos = tranimg->height - (y * nonOverlap + overlap);
    if (xpos >= tranimg->width - window) xpos = tranimg->width - window;
    if (ypos < 0) ypos = 0;
    //fprintf(stderr, "%d, %d\n", xpos, ypos);

    for (int i = 0; i < window; i++){
        for (int k = 0; k < window; k++){
            Pixel * p = &tranimg->GetPixel(xpos + k, ypos + y);
            Pixel q = block[i * window + k];

            if (avg){
                float xz = tranimg->averageLum - p->Luminance();
                plum = pow(xz, 2);
                if (xz < 0) {
                    plum = plum * -1;
                }
                float yz = averageLum - q.Luminance();
                qlum = pow(yz, 2);
                if (yz < 0) {
                    qlum = qlum * -1;
                }
            }
            //luminance += pow(p->Luminance() - q.Luminance(), 2);
            luminance += pow(((avg) ? plum : p->Luminance()) - ((avg) ? qlum : q.Luminance()), 2);
        }
    }


    return luminance;

}
Pixel * Image::Search_blocks_for_match(Image * img, Pixel * prevBlock, Pixel * topBlock, std::vector<Pixel *> allBlocks, 
                                        int type, int pos, int y, int window, bool min, int overlap, int nonOverlap,
                                        Image * tranimg, bool trans, float alpha) {
    Pixel * block;
    std::vector<Pixel *> acceptableBlocks; // this should be a vector
    int lowestIndex = 0;
    float errors[allBlocks.size()][3];
    float lowestError = std::numeric_limits<float>::infinity();

    for (int i = 0; i < allBlocks.size(); i++) {
        float e1 = (type != TOPONLY) ? findError(prevBlock, allBlocks[i], LEFTONLY, window, nonOverlap) : -1;
        float e2 = (type != LEFTONLY) ? findError(topBlock, allBlocks[i], TOPONLY, window, nonOverlap) : -1;
        float l1 = 0;

        if (trans) {
            l1 = findLuminance(allBlocks[i], tranimg, pos, y, window, averageLum, overlap, nonOverlap);
        }

        if (e1 == -1) {
            if ((e2 * alpha + l1 * (1 - alpha)) < lowestError && e2 > 0) {
                lowestIndex = i;
                lowestError = e2 * alpha + l1 * (1 - alpha);
            }
        }

        else if (e2 == -1) {
            if ((e1 * alpha + l1 * (1 - alpha)) < lowestError && e1 > 0) {
                lowestIndex = i;
                lowestError = e1 * alpha + l1 * (1 - alpha);
            }
        }

        else if (((e1 + e2) * alpha + l1 * (1 - alpha)) < lowestError && e1 > 0 && e2 > 0) {
            lowestIndex = i;
            lowestError = (e1 + e2) * alpha + l1 * (1 - alpha);
        }

        errors[i][0] = e1;
        errors[i][1] = e2;
        errors[i][2] = l1;
    }

    float thresh = lowestError * 1.2;

    for (int i = 0; i < allBlocks.size(); i++){
        float e1 = errors[i][0];
        float e2 = errors[i][1];
        float l1 = errors[i][2];

        if (e1 == -1) {
            if ((e2 * alpha + (1 - alpha) * l1) < thresh && e2 > 0) {
                acceptableBlocks.push_back(allBlocks[i]);
            }
        }

        else if (e2 == -1) {
            if ((e1 * alpha + (1 - alpha) * l1) < thresh && e1 > 0) {
                acceptableBlocks.push_back(allBlocks[i]);
            }
        }

        else if (((e1 + e2) * alpha + (1 - alpha) * l1) < thresh && e1 > 0 && e2 > 0) {
            acceptableBlocks.push_back(allBlocks[i]);
        }
    }

    
    block = acceptableBlocks[rand() % acceptableBlocks.size()];

    if (!min) {
        img->addLayeredBlock(block, pos, y, window, type, overlap, nonOverlap);
    }
    else { 
        img->addCutLayeredBlock(block, pos, y, window, type, overlap, nonOverlap);
    }
    acceptableBlocks.clear();

    return block;
}

Image* Image::structuredQuilt(int window, int newWidth, int newHeight, bool min, Image* tranimg, bool trans, float overlap_i, float alpha){
    // Get overlap and non-overlap lengths
    int overlap = window * (overlap_i / 6.0);
    overlap = std::max(overlap, 1);
    int nonOverlap = window - overlap;

    Image * img = perfectLayeredImage(newWidth, newHeight, window, overlap, nonOverlap);
    int numX = ((img->width - overlap) / nonOverlap);
    int numY = ((img->height - overlap) / nonOverlap);
    // Edge cases: Top row only has left, Left row only has top, Top Left has nothing
    std::vector<Pixel *> allBlocks;
    for (int y = 0; y < height - window; y++){
        for (int x = 0; x < width - window; x++){
            allBlocks.push_back(getBlock(x, y, window));
        }
    }

    //fprintf(stderr, "Done with allblocks\n");

    // Pick random block to start
    Pixel * prevBlock = getBlock(rand() % (width - window), rand() % (height - window), window);
    img->addBlock(prevBlock, 0, 0, window); // CHANGE THIS
    

    // To store the blocks above
    std::vector<Pixel *> toprow;
    toprow.push_back(prevBlock);

    //fprintf(stderr, "Done with firstblock\n");
    // Handle first row
    for (int x = 1; x < numX; x++){
        prevBlock = Search_blocks_for_match(img, prevBlock, prevBlock, allBlocks, LEFTONLY, x, 0, window, min, overlap, nonOverlap, tranimg, trans, alpha);
        toprow.push_back(prevBlock);
    }
    //fprintf(stderr, "%i %i YES\n", toprow.size(), allBlocks.size());


    //fprintf(stderr, "Done with toprow\n");
    for (int y = 1; y < numY; y++){
        clock_t start = clock();
        prevBlock = Search_blocks_for_match(img, prevBlock, toprow[0], allBlocks, TOPONLY, 0, y, window, min, overlap, nonOverlap, tranimg, trans, alpha);
        toprow[0] = prevBlock;
        for (int x = 1; x < numX; x++){
            prevBlock = Search_blocks_for_match(img, prevBlock, toprow[x], allBlocks, BOTH, x, y, window, min, overlap, nonOverlap, tranimg, trans, alpha);
            toprow[x] = prevBlock;
        }
        clock_t end = clock();
        printProgress(y, (end - start) / (double) CLOCKS_PER_SEC, numY);
    }

    std::cout << "\n";
    //fprintf(stderr, "Done with rest\n");
    return cropImage(img, newWidth, newHeight);
}
