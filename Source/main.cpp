#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <FreeImage.h>
#include "image.h"
#include <ctime>
#include <iostream>


/**
 * OS specific
 **/
#define OS_UNIX 1
#define OS_WIN 2

#ifdef USE_UNIX
#   define OS OS_UNIX
#else
#   define OS OS_WIN
#endif

#if OS == OS_WIN
#   include <io.h>
#   include <fcntl.h>
#endif

enum {
    RANDOM,
    SYNTH,
    ANALOGIES,
    STRUCTURED,
    MINCUT,
    TTRANSFER
};


/**
 * prototypes
 **/
static void ShowUsage(void);
static void SetBinaryIOMode(void);


/**
 * main
 **/
int main(int argc, char **argv)
{

    // look for help
    for (int i = 0; i < argc; i++) {
        if (!strcmp(argv[i], "-help") || argc < 3) {
            ShowUsage();
            return 0;
        }
    }

    // first argument is program name
    argv++, argc--;

    FreeImage_Initialise();
    FREE_IMAGE_FORMAT ftype = FIF_BMP, ftypeTran = FIF_BMP;
    int command = SYNTH, window = 10, width, height;
    float overlap = 2.0f, alpha = 0.6;
    std::string outputfile, inputfile, transferfile;
    FIBITMAP *bitmap; 
    FIBITMAP *tranbitmap;
    bool widthset = false, heightset = false, trans = false;

    srand(time(NULL));

    Image *img = NULL;
    Image *tranimg = NULL;

    // setup
    SetBinaryIOMode();

    // Handle the actual command
    if (**argv == '-'){
        // assign command
        if (!strcmp(*argv, "-random")){
            command = RANDOM;
        }
        else if (!strcmp(*argv, "-mid")){
            command = STRUCTURED;
        }
        else if (!strcmp(*argv, "-min")){
            command = MINCUT;
        }
        else {
            fprintf(stderr, "Please select type: -random, -mid, -min\n");
            ShowUsage();
            return 0;
        }

        argv++, argc--;
    }

    while (**argv == '-') { 
        if (!strcmp(*argv, "-window")){
            argv++, argc--;
            window = atoi(*argv);
            argv++, argc--;
        }
        if (!strcmp(*argv, "-width")){
            argv++, argc--;
            width = atoi(*argv);
            widthset = true;
            argv++, argc--;
        }
        if (!strcmp(*argv, "-height")){
            argv++, argc--;
            height = atoi(*argv);
            heightset = true;
            argv++, argc--;
        }
        if (!strcmp(*argv, "-overlap")){
            argv++, argc--;
            overlap = atof(*argv);
            argv++, argc--;
        }
        if (!strcmp(*argv, "-alpha")){
            argv++, argc--;
            alpha = atof(*argv);
            argv++, argc--;
        }
        if (!strcmp(*argv, "-map")){
            argv++, argc--;
            transferfile = *argv;
            argv++, argc--;
            trans = true;
            ftypeTran = FreeImage_GetFileType(transferfile.c_str());

            if (ftypeTran == FIF_UNKNOWN){
                ftypeTran = FreeImage_GetFIFFromFilename(transferfile.c_str());
            }
            tranbitmap = FreeImage_Load(ftypeTran, transferfile.c_str(), 0);
            width = FreeImage_GetWidth(tranbitmap);
            height = FreeImage_GetHeight(tranbitmap);
            tranimg = new Image(width, height);
            tranimg->Update(tranbitmap);
            widthset = true;
            heightset = true;

        }

    }
    // Read input file
    inputfile = *argv;
    outputfile = "synth" + inputfile;

    ftype = FreeImage_GetFileType(inputfile.c_str());

    if (ftype == FIF_UNKNOWN){
        ftype = FreeImage_GetFIFFromFilename(inputfile.c_str());
    }

    if (ftype != FIF_UNKNOWN){
        bitmap = FreeImage_Load(ftype, inputfile.c_str(), 0);
    }
    else {
        fprintf(stderr, "File could not be read\n");
        return 0;
    }

    argv++, argc--;

    //FreeImage_ConvertTo8Bits(bitmap);
    img = new Image(FreeImage_GetWidth(bitmap), FreeImage_GetHeight(bitmap));
    img->Update(bitmap);


    if (!widthset ) width  = 2* FreeImage_GetWidth (bitmap);
    if (!heightset) height = 2* FreeImage_GetHeight(bitmap);

    if (width < FreeImage_GetWidth(bitmap) || width > 20000) {
        fprintf(stderr, "Illegal width\n");
        ShowUsage();
        return 0;
    }
    if (height < FreeImage_GetHeight(bitmap) || height > 20000) {
        fprintf(stderr, "Illegal height\n");
        ShowUsage();
        return 0;
    }
    if (window < 3 || window > 30) {
        fprintf(stderr, "Illegal window width\n");
        ShowUsage();
        return 0;
    }
    if (alpha < .3 || alpha > 1) {
        fprintf(stderr, "Illegal alpha\n");
        ShowUsage();
        return 0;
    }
    if (overlap < 1 || overlap > 5) {
        fprintf(stderr, "Illegal overlap\n");
        ShowUsage();
        return 0;
    }

    Image * newimg = new Image(width, height);
    // Do command
    switch(command){
        case (RANDOM)       : newimg = new Image(*(img->randomQuilt(window, width, height)));
                              break;
        case (STRUCTURED)   : newimg = new Image(*(img->structuredQuilt(window, width, height, false, tranimg, trans, overlap, 1)));
                              break;
        case (MINCUT)       : newimg = new Image(*(img->structuredQuilt(window, width, height, true, tranimg, trans, overlap, (trans) ? alpha : 1)));
                              break;

        default       : *newimg = Image(*img); 
                        break;
    }

    int pitch = ((((24 * width) + 23) / 24) * 3);
    FIBITMAP *newbmp = FreeImage_ConvertFromRawBits(newimg->GetBytes(), width, height, pitch, 24, 0xFF0000, 0x00FF00, 0x0000FF, true);

    if (argc > 0){
        outputfile = *argv;
        argv++, argc--;
    }

    // write image
    if (img != NULL) // how are we doing this?
    {
        FreeImage_Save(ftype, newbmp, outputfile.c_str(), 0);
        delete img;
    }

    // done
    FreeImage_Unload(bitmap);
    FreeImage_DeInitialise();
    return EXIT_SUCCESS;
}

/**
 * ShowUsage
 **/
static char options[] =
"-help\n"
"\nTYPES\n"
"-random (randomly choose blocks)\n"
"-mid (choose blocks based on creating minimal error in the overlap)\n"
"-min (same as mid, then when applying blocks, follow the path of minimal error)\n"
"\nOPTIONS\n"
"-window d (the size of the block : default = 10 : 2 < d < 30)\n"
"-width d (the width of the output image : default = 2 * inputfile's width : must be bigger than inputfile's width)\n"
"-height d (the height of the output image : default = 2 * inputfile's height : must be bigger than inputfile's height)\n"
"-overlap f (the amount of overlap between blocks in sixths : default = 1 : 1 <= f <= 5)\n"
"-map file (map the generated texture to the luminance of the provided file)\n"
"-alpha f (the dependence on an mapped file :  default = .8 : 0.3 <= f <= 1 : a higher number relies less on the map)\n"
;

static void ShowUsage(void)
{
    fprintf(stderr, "\nUsage: ./textsyn -type [options] inputfile [outputfile]\n");
    fprintf(stderr, "\n\n%s\n\n", options);
    exit(EXIT_FAILURE);
}


/**
 * SetBinaryIOMode
 *
 * In WindowsNT by default stdin and stdout are opened as text files.
 * This code opens both as binary files.
 **/
static void SetBinaryIOMode(void)
{
#if OS == OS_WIN
    int result;

    result = _setmode(_fileno(stdin), _O_BINARY);
    if (result == -1)
    {
        perror("Cannot set stdin binary mode");
        exit(EXIT_FAILURE);
    }

    result = _setmode(_fileno(stdout), _O_BINARY);
    if (result == -1)
    {
        perror("Cannot set stdout binary mode");
        exit(EXIT_FAILURE);
    }
#endif
}

