CC=g++ -O3 -std=c++11
CFLAGS=-DUSE_UNIX -I./Includes -g -Wall
ifeq ($(shell sw_vers 2>/dev/null | grep Mac | awk '{ print $$2}'),Mac)
LDFLAGS = -L./lib/mac/ -lm -lstdc++ -lfreeimage
else
LDFLAGS = -lfreeimage
endif

all: textsyn
textsyn: image.o pixel.o main.o
	$(CC) $(CFLAGS) -o textsyn image.o pixel.o main.o $(LDFLAGS)

main.o: Source/main.cpp Includes/image.h 
	$(CC) $(CFLAGS) -c Source/main.cpp
image.o: Source/image.cpp Includes/image.h Includes/pixel.h 
	$(CC) $(CFLAGS) -c Source/image.cpp
pixel.o: Source/pixel.cpp Includes/pixel.h 
	$(CC) $(CFLAGS) -c Source/pixel.cpp
clean:
	-rm -f textsyn
	-rm -f Source/*.o
