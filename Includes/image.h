/**
 * Image.h
 *
 * Image class.
 *
 * original by Wagner Correa at Princeton, 1999
 * turned to C++ by Robert Osada, 2000
 * Slight modifications for 4162 assignment in Spring 2005.
 **/
#ifndef IMAGE_INCLUDED
#define IMAGE_INCLUDED

#include <assert.h>
#include <stdio.h>
#include "pixel.h"
#include <FreeImage.h>
#include <vector>
#include <string.h>
#include <limits>

/**
 * Image
 **/
class Image
{
private:
    void addBlock(Pixel* block, int xIndex, int yIndex, int window);
    void addLayeredBlock(Pixel* block, int xIndex, int yIndex, int window, int type, int overlap, int nonOverlap);
    void addCutLayeredBlock(Pixel* block, int xIndex, int yIndex, int window, int type, int overlap, int nonOverlap);

    Pixel* getBlock(int x, int y, int window);
public:
    Pixel *pixels;
    int width, height, num_pixels;
    float averageLum;

    // Creates a blank image with the given dimensions
    Image (int width, int height);

    // Copy iamage
    Image (const Image& src);

    // Destructor
    ~Image ();

    // Pixel access
    int ValidCoord (int x, int y)  { return x>=0 && x<width && y>=0 && y<height; }
    Pixel& GetPixel (int x, int y) { assert(ValidCoord(x,y)); return pixels[y*width + x]; }
    void Update(FIBITMAP* bitmap);
    unsigned char * GetBytes();

    Image * cropImage(Image * img, int w, int h);
    Image * perfectImage(int w, int h, int window);
    Image * perfectLayeredImage(int w, int h, int window, int overlap, int nonOverlap);

    // Dimension access
    int Width     () { return width; }
    int Height    () { return height; }
    int NumPixels () { return num_pixels; }

    Image * randomQuilt(int window, int newWidth, int newHeight);
    Image * structuredQuilt(int window, int newWidth, int newHeight, bool min, Image* tranimg, bool trans, float overlap, float alpha);
    Pixel * Search_blocks_for_match(Image * img, Pixel * prevBlock, Pixel * topBlock, std::vector<Pixel *> allBlocks, int type, int pos, int y, int window, bool min, int overlap, int nonOverlap, Image * tranimg, bool trans, float alpha);
    float findError(Pixel * b1, Pixel * b2, int type, int window, int nonOverlap);
};

#endif
